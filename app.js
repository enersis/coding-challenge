// #########################################################
// Too many responsibilities in this file. Can you fix it? #
// #########################################################

var express = require("express");
const Sequelize = require('sequelize'); 

var app = express();

app.use("/static", express.static("static"));

app.get("/", function (req, res) {
  res.send("enersis coding challenge");
});

const sequelize = new Sequelize("enersis", "postgres", "postgres", {
    dialect: 'postgres'
});

app.get("/countries", function (req, res) {
    sequelize.query('SELECT * FROM test.countries', { type: sequelize.QueryTypes.SELECT})
        .then(function(results) {
            res.json({results: results});
    });
    // Error Handling missing. Can you fix it?
});

app.listen(3000, function () {
  console.log("app listening on port 3000!");
});