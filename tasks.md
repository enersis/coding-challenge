# Fullstack developer coding challenge

Try to solve the following sub tasks in the given order. It doesn’t matter how far you get, the challenge is designed to take longer than the given time. It is ok to move to the next step if you are stuck. Start with this template: https://bitbucket.org/enersis/coding-challenge/src/master/.

## Endpoint

Create an endpoint that returns a JSON containing the sum of the salaries per department, the department_id and department_name for the three departments with the highest total sum of salaries.

## Frontend
Create a static page where the user can select a country per name and request employee salary data from the previously created endpoint. At this stage, the country names can be hardcoded and not all countries need to be supported (US, and UK are sufficient). The received data should be shown in a table. 

# Interface tests using jasmine
Add jasmine (https://jasmine.github.io/) as a new dependency to the project and create interface tests. You can assume that the database is available at localhost:5432.

## Styling
Time for a little style upgrade. The user controls should only use 80% of the screen width. Remove outside borders of the table, table lines must be dotted, remove space between cells. For every cell, the distance between content and border should change relative to the font size.
Dynamic countries
Create a new endpoint and change the UI so that the list of available countries is fetched from the database. Apply TDD/BDD by beginning with the interface test.
